Feature: Is it Positive number?
  Scenario: Subtract two numbers(Undefined)
    Given the two numbers are not given
    When the calculator is run without numbers
    Then the result should be correct but not caught

  Scenario: Add two positive numbers(Passed)
    Given the two numbers are positive
    When the positive numbers are added
    Then the result should be positive

  Scenario: Add two negative numbers(Failed)
    Given the two numbers are negative
    When the negative numbers are added
    Then the result should not be positive