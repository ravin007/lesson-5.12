package VerifyPositiveNumbers;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;

public class StepDefinitions {
    double num1;
    double num2;
    double result;

    @Given("the two numbers are positive")
    public void the_two_numbers_are_positive() {
         num1 =10;
         num2 = 5;
    }

    @When("the positive numbers are added")
    public void the_numbers_are_added() {
        result = num1 + num2;
    }
    @Then("the result should be positive")
    public void the_result_should_be_positive() {

        assertEquals("Results should match", 15,15);

    }

    @Given("the two numbers are negative")
    public void the_two_numbers_are_negative() {
        num1 =10;
        num2 = -5;
    }

    @When("the negative numbers are added")
    public void the_negative_numbers_are_added() {
         result = num1 + num2;
    }
    @Then("the result should not be positive")
    public void the_result_shouldNot_be_positive() {

        assertEquals("Results should match", -15,15);

    }


}
